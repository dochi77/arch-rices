#################################
#           Animations          #    
#################################    

animations = true;
animation-stiffness = 300.0;
animation-dampening = 28.0;
animation-clamping = false;
animation-mass = 1;
animation-for-open-window = "zoom";
animation-for-menu-window = "slide-down";
animation-for-transient-window = "slide-down";
#################################
#           Fading              #
#################################

fading = true;
fade-delta = 4;
no-fading-openclose = false;

fade-exclude = [ ];

#################################
#           Shadows             #
#################################

shadow = true;

shadow-radius = 12;

shadow-offset-x = -8;

shadow-offset-y = -8;

shadow-exclude = [
  "name = 'Notification'",
  # "class_g = 'Polybar'",
  "class_g ?= 'Notify-osd'",
  "class_g = 'Cairo-clock'",
  "_GTK_FRAME_EXTENTS@:c"
];

# Specify a list of conditions of windows that should have no shadow painted over, such as a dock window.
# clip-shadow-above = []

# Specify a X geometry that describes the region in which shadow should not
# be painted in, such as a dock window region. Use
#    shadow-exclude-reg = "x10+0+0"
# for example, if the 10 pixels on the bottom of the screen should not have shadows painted on.
#
# shadow-exclude-reg = ""

# Crop shadow of a window fully on a particular Xinerama screen to the screen.
# xinerama-shadow-crop = false




#################################
#   Transparency / Opacity      #
#################################


inactive-opacity = 1;

frame-opacity = 1.0;

inactive-opacity-override = false;


#################################
#           Corners             #
#################################

corner-radius = 12;

rounded-corners-exclude = [
  # "class_g = 'Polybar'",
]

#################################
#     Background-Blurring       #
#################################

# blur-background = true;

# blur-method = "dual_kawase";
# blur-strength = 6;

# blur-background-fixed = true;


#################################
#       General Settings        #
#################################


backend = "glx"
glx-copy-from-front = false;
vsync = true;

mark-wmwin-focused = true;

mark-ovredir-focused = true;

detect-rounded-corners = true;

detect-client-opacity = true;


unredir-if-possible = true

unredir-if-possible-exclude = [
        "class_g = 'looking-glass-client' && !focused",
        "class_g = 'Brave-browser'",
        "class_g = 'vlc'",
        "class_g = 'i3lock'",
        "class_g = 'sm64ex'",
        "class_g = 'pendulum.py'",
        "class_g = 'mpv'",
] 

detect-transient = true;

resize-damage = 1


glx-no-stencil = true;

glx-no-rebind-pixmap = true

no-use-damage = true;


xrender-sync-fence = false


no-ewmh-fullscreen = true

log-level = "warn";

wintypes:
{
  normal = { full-shadow = true; };
  tooltip = { fade = true; shadow = true; opacity = 0.85; focus = true; full-shadow = true; };
  dock = { full-shadow = true; clip-shadow-above = true;}
  dnd = { shadow = false; }
  notification = { full-shadow = true; }
}
