#!/bin/bash

style=$1

cp -r ./$1/config/ ~/.config/
cp ./$1/lvim/colors.lua ~/.local/share/lunarvim/site/pack/packer/start/tokyonight.nvim/lua/tokyonight/


bspc config top_padding 0 && bspc config bottom_padding 0

bspc wm -r

# spicetify config current_theme $style && spicetify apply

betterlockscreen -u ~/Pictures/WallPapers/$style/lockscreen/$style.png 

echo "You now have a $style rice!"
