#!/usr/bin/env bash

spotify --no-zygote &
pid_spotify=$!


PATH_TO_CONFIG=~/.config/polybar/fail/config.ini

polybar -c $PATH_TO_CONFIG -r spotify &
pid_polybar=$!

wait $pid_spotify

kill $pid_polybar
