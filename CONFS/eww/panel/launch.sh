#!/bin/bash

# enter where you have your eww binary (leave it like this if you have it in your path)

# enter your directory

eww="eww -c $HOME/.config/eww/panel"

# launch


lock="/tmp/panel.lock"
if [ ! -f "$lock" ]; then
  touch "$lock"
  $eww close-all
else
  rm "$lock"
  $eww open panel
  sleep 0.1
fi
 
