#!/bin/bash

Control="spotify"

########################## Title ##########################
title() {
title=$(playerctl --player=$Control metadata --format {{title}})
[ -z "$title" ] && title="Nothing Playing"
echo "$title"
}

########################## Time ##########################

ctime() {
	time=$(playerctl --player=$Control position --format "{{ duration(position) }}")
	[ -z "$time" ] && time="0:00"
	echo "$time"
}

time_perc() {
  if [ "$(playerctl --player=$Control status)" == "No players found" ]; then
    # percentage=0
    echo 0

  else 
    time_str=$(playerctl -p spotify metadata -f "{{duration(mpris:length)}}")
    minutes=${time_str%:*}
    seconds=${time_str#*:}
    total_time=$((minutes * 60 + seconds))

    time_str=$(~/.config/eww/panel/scripts/music_info.sh time)
    minutes=${time_str%:*}
    seconds=${time_str#*:}
    position=$((minutes * 60 + seconds))

    percentage=$((100 * position / total_time))
    echo $percentage
 fi
}

########################## Length ##########################
length() {
	length=$(playerctl --player=$Control metadata --format "{{ duration(mpris:length) }}")
	[ -z "$length" ] && length="0:00"
	echo "$length"
}

########################## Artist ##########################
artist() {
artist=$(playerctl --player=$Control metadata --format {{artist}})
[ -z "$artist" ] && artist="Offline"
echo "$artist"
}

########################## Status ##########################
status() {
status=$(playerctl --player=$Control status)
[ -z "$status" ] && status="Stopped"
echo "$status"
}

########################## Art ##########################

get_song_art () {
  TMP_DIR="$HOME/.cache/eww"
  TMP_COVER_PATH=$TMP_DIR/cover.png
  TMP_TEMP_PATH=$TMP_DIR/temp.png

  if [[ ! -d $TMP_DIR ]]; then
    mkdir -p $TMP_DIR
  fi

  ART_FROM_SPOTIFY="$(playerctl -p %any,spotify metadata mpris:artUrl | sed -e 's/open.spotify.com/i.scdn.co/g')"
  ART_FROM_BROWSER="$(playerctl -p %any,mpd,firefox,chromium,brave metadata mpris:artUrl | sed -e 's/file:\/\///g')"

  if [[ $(playerctl -p spotify,%any,firefox,chromium,brave,mpd metadata mpris:artUrl) ]]; then
    curl -s "$ART_FROM_SPOTIFY" --output $TMP_TEMP_PATH
  elif [[ -n $ART_FROM_BROWSER ]]; then
    cp $ART_FROM_BROWSER $TMP_TEMP_PATH
  else
    cp $HOME/.config/eww/panel/assets/fallback.png $TMP_TEMP_PATH
  fi

  convert $TMP_TEMP_PATH -gravity center +repage -alpha set -channel A \
    -sparse-color Barycentric '%[fx:w*0.1/32],0 transparent  %[fx:w+1.0],0 opaque' \
    -evaluate multiply 0.45 \
    $TMP_COVER_PATH
}

echo_art () {
  get_song_art
  echo "$HOME/.cache/eww/cover.png"
}

########################## Statusicon ##########################

statusicon () {
  STATUS=$(playerctl -p spotify status)
  if [ "$STATUS" = "Playing" ]; then
    echo ""
  elif [ "$STATUS" = "Paused" ]; then
    echo ""
  else
    echo ""
  fi
}

if [ "$1" = "title" ]; then
  title
elif [ "$1" = "icon" ]; then
  statusicon
elif [ "$1" = "artist" ]; then
  artist
elif [ "$1" = "time" ]; then
  ctime
elif [ "$1" = "length" ]; then
  length
elif [ "$1" = "time-percent" ]; then
  time_perc
elif [ "$1" = "cover" ]; then
  echo_art
fi
