#!/bin/bash

hours=$(echo $(uptime -p) | awk -F'[ :]' '{print $2}')
minutes=$(echo $(uptime -p) | awk -F'[ :]' '{print $4}')

echo "$hours:$minutes"

