#!/bin/bash

eww="eww -c $HOME/.config/eww/dashboard"
ewwm="eww -c $HOME/.config/eww/dashboard/music"
lock="/tmp/dashboard.lock"

if [ ! -f "$lock" ]; then
  touch "$lock"
  $eww close-all
  $ewwm close-all
else
  rm "$lock"
  $eww open-many profile weather clock apps system quotes powermenu
  $ewwm open music
fi
 
