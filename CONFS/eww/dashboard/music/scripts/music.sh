#!/bin/bash

Control="spotify"

########################## Title ##########################
title() {
title=$(playerctl --player=$Control metadata --format {{title}})
[ -z "$title" ] && title="Nothing Playing"
echo "$title"
}

########################## Artist ##########################
artist() {
artist=$(playerctl --player=$Control metadata --format {{artist}})
[ -z "$artist" ] && artist="Offline"
echo "$artist"
}

########################## Status ##########################
status() {
status=$(playerctl --player=$Control status)
[ -z "$status" ] && status="Stopped"
echo "$status"
}

########################## Time ##########################
ctime() {
time=$(playerctl --player=$Control position --format "{{ duration(position) }}")
[ -z "$time" ] && time="0:00"
echo "$time"
}

time_perc() {
  if [ "$(playerctl --player=$Control status)" == "No players found" ]; then
    percentage=0
    echo $percentage

  else 
    time_str=$(playerctl -p spotify metadata -f "{{duration(mpris:length)}}")
    minutes=${time_str%:*}
    seconds=${time_str#*:}
    total_time=$((minutes * 60 + seconds))

    time_str=$(~/.config/eww/panel/scripts/music_info.sh time)
    minutes=${time_str%:*}
    seconds=${time_str#*:}
    position=$((minutes * 60 + seconds))

    percentage=$((100 * position / total_time))
    echo $percentage
 fi
}

########################## Length ##########################
length() {
length=$(playerctl --player=$Control metadata --format "{{ duration(mpris:length) }}")
[ -z "$length" ] && length="0:00"
echo "$length"
}

########################## Cover ##########################

Cover=/tmp/cover.png
bkpCover=~/.config/eww/dashboard/music/fallback.png

cover() {
albumart="$(playerctl --player=$Control metadata mpris:artUrl | sed -e 's/open.spotify.com/i.scdn.co/g')"
[ $(playerctl --player=$Control metadata mpris:artUrl) ] && curl -s "$albumart" --output $Cover || cp $bkpCover $Cover 
echo "$Cover"
}

########################## Statusicon ##########################
# statusicon() {
# icon=""
# [ $(playerctl --player=$Control status) = "Playing" ] && icon=""
# [ $(playerctl --player=$Control status) = "Paused" ] && icon=""
# echo "$icon"
# }

statusicon () {
  STATUS=$(playerctl -p spotify status)
  if [ "$STATUS" = "Playing" ]; then
    echo ""
  elif [ "$STATUS" = "Paused" ]; then
    echo ""
  else
    echo ""
  fi
}

if [ "$1" = "title" ]; then
  title
elif [ "$1" = "icon" ]; then
  statusicon
elif [ "$1" = "artist" ]; then
  artist
elif [ "$1" = "art" ]; then
  cover
elif [ "$1" = "time" ]; then
  ctime
elif [ "$1" = "time-percent" ]; then
  time_perc

fi
