#!/bin/bash

# Enter your eww directory, leave as it is if you have copied.

eww="eww -c $HOME/.config/eww/bar"

# Launch

lock="/tmp/bar.lock"
if [ ! -f "$lock" ]; then
  touch "$lock"
  $eww close-all
else
  rm "$lock"
  $eww open bar
fi
 
